import { Alphabet } from "./alphabet";
import { Dictionary } from "./dictionary";

declare const require: any;
const Enigma = require("node-enigma");

const Rotors = ["i", "ii", "iii", "iv", "v", "vi", "vii", "viii"];
const Reflectors = ["ukw-b", "ukw-c"];

const encryptedCode =
  "VJKIINYNJCQYJOSIQGSRDADWCEFFIRPZFKJZTEYXVZNVRJLHBLTAKSNLAVOBTGTVGIIUDDUVNNQASFWQMJCCARLXC";
const codes = [];
for (let i = 0; i < Alphabet.length; i++) {
  const letterOne = Alphabet[i];
  for (let j = 0; j < Alphabet.length; j++) {
    const letterTwo = Alphabet[j];
    for (let k = 0; k < Alphabet.length; k++) {
      const letterThree = Alphabet[k];
      codes.push({
        letterOne,
        letterTwo,
        letterThree
      });
    }
  }
}

const settings = [];
for (let i = 0; i < Rotors.length; i++) {
  const rotorOne = Rotors[i];
  for (let j = 0; j < Rotors.length; j++) {
    const rotorTwo = Rotors[j];
    for (let k = 0; k < Rotors.length; k++) {
      const rotorThree = Rotors[k];
      for (let z = 0; z < Reflectors.length; z++) {
        const reflector = Reflectors[z];
        if (
          rotorOne !== rotorTwo &&
          rotorOne !== rotorThree &&
          rotorTwo !== rotorThree
        ) {
          settings.push({
            rotorOne,
            rotorTwo,
            rotorThree,
            reflector
          });
        }
      }
    }
  }
}

for (let i = 0; i < codes.length; i++) {
  for (let j = 0; j < settings.length; j++) {
    const m3 = new Enigma(
      settings[j].rotorOne,
      settings[j].rotorTwo,
      settings[j].rotorThree,
      settings[j].reflector
    );
    m3.setCode([codes[i].letterOne, codes[i].letterTwo, codes[i].letterThree]);
    m3.setPlugboard({
      'A': 'Z',
      'D': 'E',
      'E': 'D',
      'F': 'G',
      'G': 'F',
      'L': 'M',
      'M': 'L',
      'Z': 'A'
    });
    const decoded = m3.decode(encryptedCode);
    console.log(decoded);
    if (decoded.indexOf('RBLQ') !== -1) {
      console.log(decoded);
      const wordCount = countWords(decoded);
      if (wordCount > 2) {
        console.log("Number of words:", wordCount);
        console.log("Match found:", decoded);
        console.log("Letter one:", codes[i].letterOne);
        console.log("Letter two:", codes[i].letterTwo);
        console.log("Letter three:", codes[i].letterThree);
      }
    }
  }
}

function countWords(decodedString) {
  let count = 0;
  for (let i = 0; i < Dictionary.length; i++) {
    if (
      Dictionary[i].length > 3 &&
      decodedString.toLowerCase().indexOf(Dictionary[i].toLowerCase()) !== -1
    ) {
      count++;
    }
  }
  return count;
}
